@extends('data.master')
@section('title')
  Halaman Tamil
@endsection

@section('sub-title')
Halaman Tampil Cost
    
@endsection

@section ('content')

<a href="cost/create" class="btn btn-primary btn-sm">tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">Action</th>       
      </tr>
    </thead>
    <tbody>
        @forelse ($costies as $key => $item)
        <tr>
            <th scope="row">{{$key + 1 }}</th>
            <td>{{$item->nama}}</td>
            <td>
               <form action="/cost/{{$item->id}}" method="POST">
                <a href="/cost/{{$item->id}}" class="btn btn-info btn-sm">Detail </a>
                <a href="/cost/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit </a>
                
                  @csrf
                  @method('delete')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">

            </td>
        </tr>      
        @empty
        <h1>Data Kosong</h1>
            
        @endforelse
     
    </tbody>
  </table>

@endsection