@extends('data.master')
@section('title')
  Halaman Edit
@endsection

@section('sub-title')
Halaman Edit Cost
    
@endsection

@section ('content')

<form action="/cost/{{$costt->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>nama</label>
      <input  type="text" name = 'nama' value="{{$costt->nama}}" class="form-control">
         </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>umur</label>
    <input type="string"  name = 'umur' value="{{$costt->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
       <label >bio</label>
       <input  type="text" name = 'bio' value="{{$costt->bio}}" class="form-control">
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection