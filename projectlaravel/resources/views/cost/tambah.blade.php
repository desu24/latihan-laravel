@extends('data.master')
@section('title')
  Halaman Cost
@endsection

@section('sub-title')
Halaman Tambah Cost
    
@endsection

@section ('content')

<form action="/cost" method="post">
    @csrf
    <div class="form-group">
      <label>nama</label>
      <input name = 'nama' type="text" class="form-control">
         </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>umur</label>
    <input name = 'umur' type="string" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
       <label >bio</label>
       <input name = 'bio' type="text" class="form-control">
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection