<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Input From</title>
</head>
<body>
    <h2> Input Form </h2>

    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="name1"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="name2"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="status"> Male <br>
        <input type="radio" name="status"> Female <br>
        <input type="radio" name="status"> Other <br> <br>             
        <label>Nationality:</label><br><br>
        <select name="negara">
        <option value="Indonesia">Indonesia</option> <br>
        <option value="Singapure">Singapure</option> <br>
        <option value="Malaysia">Malaysia</option> <br>
        <option value="Korea">Korea</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" value="1" name="language">Bahasa Indonesia<br>
        <input type="checkbox" value="2" name="language">English<br>
        <input type="checkbox" value="3" name="language">Other<br><br>  
        <label>Bio:</label><br><br>                       
        <textarea name="Bio" rows="10" cols="30"></textarea><br><br>               
        <input type="submit" value="Sign Up">
    </form>    
</body>
</html>