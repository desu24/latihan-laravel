<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/table', [HomeController::class, 'home']);
Route::get('/registrasi', [AuthController::class, 'regis']);
Route::post('/welcome', [AuthController::class, 'kirim']);

//testing template
Route::get('/data-table', function () {
    return view('data.data');
});

//CRUD Catagory
//Create Catagory
//Route untuk mengarah ke input table Cost
Route::get('cost/create', [CostController::class, 'create']);
//Route untuk menyimpan ke database table cost
route::post('/cost', [CostController::class, 'costs']);
//Route Tampilkan Data
Route::get('/cost', [CostController::class, 'index']);
Route::get('/cost/{id}', [CostController::class, 'show']);

//Update Data
route::get('/cost/{id}/edit', [CostController::class, 'edit']);
route::put('/cost/{id}', [CostController::class, 'update']);

//Delete Data table Cost 'nama fungsi nya jangan delete'
route::delete('/cost/{id}', [CostController::class, 'destroy']);
