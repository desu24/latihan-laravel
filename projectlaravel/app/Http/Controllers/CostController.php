<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CostController extends Controller
{
    public function create()
    {
        return view('cost.tambah');
    }
    public function costs(Request $request)
    {
        //vslidasi controller
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cost')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cost');
    }
    public function index()
    {
        $costies = DB::table('cost')->get();

        return view('cost.tampil', ['costies' => $costies]);
    }
    public function show($id)
    {
        $costt = DB::table('cost')->find($id);
        return view('cost.detail', ['costt' => $costt]);
    }

    public function edit($id)
    {
        $costt = DB::table('cost')->find($id);
        return view('cost.edit', ['costt' => $costt]);
    }

    public function update(request $request, $id)
    {
        //vslidasi controller
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cost')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']

                ]
            );
        return redirect('/cost');
    }

    public function destroy($id)
    {
        DB::table('cost')->where('id', '=', $id)->delete();
        return redirect('/cost');
    }
}
