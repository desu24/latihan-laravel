<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('reg.register');
    }
    public function kirim(Request $request)
    {

        $namaDepan = $request['name1'];
        $namaBelakang = $request['name2'];
        return view('welcome.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
